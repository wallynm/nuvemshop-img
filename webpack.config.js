
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackBundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require("compression-webpack-plugin");


const bundleAnalyzerPlugin = new WebpackBundleAnalyzer();
const compressionPlugin = new CompressionPlugin();

const extractSassChunksPlugin = new ExtractTextPlugin({
  filename: 'css/[name].css',
  allChunks: true
});

const htmlWebpackPlugin = new HtmlWebpackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
})


const babelLoader = {
  test: /\.js$/,
  exclude: /node_modules/,
  use: {
    loader: "babel-loader"
  }
};

const scssLoader = {
  test: /\.s(a|c)ss$/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          importLoaders: 10,
          sourceMap: true,
          minimize: true
        }
      }, {
        loader: 'postcss-loader',
        options: { sourceMap: true }
      }, {
        loader: 'sass-loader',
        options: {
          sourceMap: true,
          minimize: true          
        }
      }, {
        loader: 'sass-resources-loader',
        options: {
          resources: [
            './src/configs/styles/Variables.scss',
            './src/configs/styles/Mixins.scss'
          ]
        }
      }]
  })
};

module.exports = {
  entry: './src/index.js',
  resolve: {
    modules: ['./node_modules', './src']
  },

  output: {
    publicPath: '/',
    path: __dirname + '/dist',
    filename: 'index_bundle.js'
  },

  devServer: {
    historyApiFallback: true,
    compress: true,
    contentBase: './public'
  },

  plugins: [
    extractSassChunksPlugin,
    htmlWebpackPlugin,
    compressionPlugin
  ],

  module: {
    rules: [
      babelLoader,
      scssLoader
    ]
  }
};

