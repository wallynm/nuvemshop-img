import React, { Component } from 'react';
import axios from 'axios';
import convertJson from 'xml-js';

import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';

@inject('RouterStore')
@withRouter
@observer
class CatList extends Component {
  state = {
    images : []
  }

  componentDidMount() {
    this.fetchImages();
  }

  fetchImages = () => {
    let category = 'hats';
    let items = 100;

    const { params } = this.props.match;

    if(params.category) {
      category = params.category
      if(category === 'hats') {
        items = 20;
      }

      if(category === 'space') {
        items = 50;        
      }
    }

    axios.get(`http://thecatapi.com/api/images/get?format=xml&results_per_page=${items}&category=${category}`)
    .then((response) => {
      const json = convertJson.xml2json(response.data, {compact: true, spaces: 4});
      const data = JSON.parse(json);
      this.setState({images: data.response.data.images.image });
    });

  }

  renderListItems = () => {
    const { images } = this.state;

    console.info(images)

    return images.map((image, key) => {

      return (
        <li key={key}>
          <img height={60} src={image.url._text}/>
        </li>
      );
    });
  }

  render() {
    const items = this.renderListItems();

    if(!items){
      return null;
    }

    return (
    <ul>
      {items}
    </ul>);
  }
}

export default CatList;