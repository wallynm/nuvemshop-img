import { observable } from 'mobx';

class UserStore {
  @observable userName = 'Martín Palombo';
  @observable storeName = 'Nome da minha loja';
  @observable storeLink = 'teste-brasil.lojavirtual.seudominino.lojavirtual.com.br.compremania';
  @observable storeLogo = 'https://image.ibb.co/dBScXd/Captura_de_Tela_2018_05_31_a_s_14_51_07.png';
  @observable email = null;
  @observable id = null;
}

export default new UserStore();