
import { CatList } from 'modules/cats';

console.info('CatList', CatList)

const Routes = [{
  private: false,
  path: '/cats',
  title: 'Meus Produtos',
  layout: 'logged',
  component: CatList
}, {
  private: false,
  path: '/cats/:category',
  title: 'Cats',  
  layout: 'logged',
  component: CatList
}];


export default Routes;
