import { observable } from 'mobx';

class AppStore {
  @observable sidebarOpen = true;
}

export default new AppStore();