
import mobx, { computed, observable } from "mobx"
import AutoStore from "configs/AutoStore"

class ProductStore {
  constructor() {
    AutoStore(this);
  }

  @observable products = [];
}

export default new ProductStore();