import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Link } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';

import './ProductList.scss';

import { Grid } from 'components/grid';
import { Table } from 'components/table'; 
import { Checkbox } from 'components/input';
import { Button, ButtonIcon } from 'components/button';
import { Image } from 'components/image';
import { ProductService } from 'modules/product';

@inject('ProductStore')
@observer
class ProductList extends Component {
  state = {
    page: 0,
    maxItems: 8
  }

  changePage = page => {
    this.setState({
      page
    })
  }

  componentWillReact() {
    const maxPages = this.maxPages;
    const { page, maxItems } = this.state;    
    const { products } = this.props.ProductStore;

    console.info(maxPages !== page && (typeof products[page * maxItems] === 'undefined'));
    if((maxPages !== page) && typeof products[page * maxItems] === 'undefined'){
      this.setState({page: maxPages});
    }
  }

  get maxPages() {
    const { products } = this.props.ProductStore;
    const { maxItems } = this.state;    
    return Math.ceil(products.length / (maxItems)) - 1;
  }

  paginationRender() {
    const numPages = this.maxPages;
    const pages = [];
    const { page } = this.state;
    
    for (let i = 0; i <= numPages; i++ ) {
      pages.push(<Button outline={page === i} onClick={() => this.changePage(i)} className="mr--md">{i + 1}</Button>);
    }

    if(pages.length === 1) {
      return null;
    }

    return pages;
  }

  productsRender() {
    const { products } = this.props.ProductStore;
    const { page, maxItems } = this.state;
    const talbeRows = []

    for(let i = 0; i < maxItems; i++) {
      const index = (page * maxItems) + i;
      const item = products[index];
      const image = (item && item.images) ? item.images[0] : null;
      
      if(item) {
        const row = (
          <tr key={index}>
            <td><Checkbox name={`select-product-${index}`}/> </td>
            <td>
              <div className="product--detail">
                <Image bg={image}/>
                <Link to={this.productEditPath(item.id)}>{item.name}</Link>
              </div>
            </td>
            <td>{item.stock}</td>
            <td>R$ {item.price}</td>
            <td>R$ {item.promotionalPrice}</td>
            <td>{item.price}</td>
            <td>
              <Link to={this.productEditPath(item.id)}><ButtonIcon size="small" transparent icon='edit'>Editar</ButtonIcon></Link>
              <ButtonIcon size="small" onClick={() => ProductService.removeProduct(item.id)} transparent icon='delete'>Apagar</ButtonIcon>
            </td>
          </tr>
        );
  
        talbeRows.push(row);
      }
    }

    return talbeRows;
  }

  productEditPath(id){
    return `/products/edit/${id}`;
  }

  render() {
    const { products } = this.props.ProductStore;    

    if(isEmpty(products)){
      return (
        <div className="empty-list">
          Você ainda não possui nenhum produto cadastrado, 
          crie um <Link to="/products/new">novo produto</Link> primeiro
        </div>
      );
    }

    return (
      <div>
        <Grid nopadding>
          <Table>
            <thead>
              <tr>
                <th width={40} className="all">
                  <Checkbox name="select-all"/>
                </th>
                <th width={300}>Produto</th>
                <th>Estoque</th>
                <th>Preço original</th>
                <th>Preço promocional</th>
                <th>Variações</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              {this.productsRender()}
            </tbody>
          </Table>
        </Grid>

        <div className="table--pagination">
          {this.paginationRender()}
        </div>
      </div>
    )
  }
}

export default ProductList;