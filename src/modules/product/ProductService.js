import ProductStore from './ProductStore';
import { generateId } from './helper';
import isEmpty from 'lodash/isEmpty';

class ProductService {
  addProduct(data) {
    if(data) {
      data.id = generateId();
      ProductStore.products.push(data);
    }
  }

  updateProduct(data) {
    const products = ProductStore.products.map(item => {
      if(item.id === data.id) {
        item = { ...item, ...data };
      }

      return item;
    });
    ProductStore.products = products;
  }

  getProduct(id) {
    return ProductStore.products.find(item => item.id === id);
  }

  removeProduct(id) {
    const products = [];
    ProductStore.products.map(item => {
      if (item.id !== id) {
        products.push(item);
      }
    });

    if(isEmpty(products)) {
      ProductStore.products = [];  
    } else {
      ProductStore.products = products;
    }
  }
}

export default new ProductService();