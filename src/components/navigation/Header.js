import React, { Component } from 'react';
import classname from 'classname';
import './Header.scss';

import { ButtonIcon } from 'components/button';
import Breadcumbs from './Breadcumbs';

import { AppService } from 'modules/app';

class Header extends Component {
  sidebarHandler = () => {
    AppService.toggleSidebar();
  }

  render() {
    const { className } = this.props;
    const classComponent = classname('header', className);
    
    return (
      <div className={classComponent}>
        <ButtonIcon transparent type='secondary' icon='menu' className={'menu--button'} onClick={this.sidebarHandler}/>
        <Breadcumbs/>
      </div>
    )
  }
};

export default Header;