import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import classname from 'classname';
import './Sidebar.scss';

import { ButtonIcon } from 'components/button';
@inject("AppStore")
@inject("UserStore")
@observer
class Sidebar extends Component {
  render() {
    const { sidebarOpen } = this.props.AppStore;
    const { userName, storeName, storeLogo, storeLink } = this.props.UserStore;
    
    const classComponent = classname('sidebar', {
      'sidebar--closed': !sidebarOpen
    });

    return (
      <div className={classComponent}>
        <div className="sidebar--header">
          <div className="store--info">
            <img src={storeLogo} height="40"/>
            <div>
              <div className="title">{storeName}</div>
              <div className="user">{userName}</div>
            </div>
          </div>
          
          <ButtonIcon clamp outline block icon='external' pos='right'>{storeLink}</ButtonIcon>
        </div>
        <div className="sidebar--content">
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default Sidebar;